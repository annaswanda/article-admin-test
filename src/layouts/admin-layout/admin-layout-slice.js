import { createSlice } from "@reduxjs/toolkit";

export const adminLayoutSlice = createSlice({
    name: 'adminLayout',
    initialState: {
        title: '',
        subTitle: '',
    },
    reducers: {
        setAdminLayoutTitle(state, payload) {
            state.title = payload.payload.title;
            state.subTitle = payload.payload.subTitle;
        }
    }
});

export const { setAdminLayoutTitle } = adminLayoutSlice.actions;
export default adminLayoutSlice.reducer;
