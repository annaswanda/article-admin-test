import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { ArticleIcon } from "../../../components/icons/article-icon";

export const SideBarList = () => {
  return (
    <>
      <ListItemButton>
        <ListItemIcon>
          <ArticleIcon />
        </ListItemIcon>
        <ListItemText primary="Article" />
      </ListItemButton>
    </>
  );
};
