import * as React from "react";
import { styled, createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import MuiDrawer from "@mui/material/Drawer";
import Box from "@mui/material/Box";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Badge from "@mui/material/Badge";
import MenuIcon from "@mui/icons-material/Menu";
import { SideBarList } from "./partials/sidebar-list";
import { Outlet } from "react-router-dom";
import { Logo } from "../../components/icons/logo";
import { useSelector } from "react-redux";
import { NotificationIcon } from "../../components/icons/notification-icon";

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  boxShadow: "unset !important",
  backgroundColor: "white !important",
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme({
  components: {
    MuiButton: {},
  },
  typography: {
    fontFamily: [
      "Open Sans",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
});

const AdminLayout = () => {
  const [open, setOpen] = React.useState(true);
  const adminLayout = useSelector((state) => state.adminLayout);
  const toggleDrawer = () => {
    setOpen(!open);
  };
  React.useEffect(() => {
    window.document.title = adminLayout.subTitle || "Admin";
  }, [adminLayout.subTitle]);

  return (
    <ThemeProvider theme={defaultTheme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar
          position="absolute"
          open={open}
          sx={{
            backgroundColor: "rgba(249, 249, 249, 1)",
            minHeight: "94px",
            color: "rgba(23, 23, 23, 1)",
          }}
        >
          <Toolbar
            sx={{
              minHeight: "94px !important",
              paddingLeft: "30px !important",
              pr: "24px", // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: "36px",
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              fontWeight={600}
              noWrap
              sx={{ flexGrow: 1 }}
            >
              {adminLayout.title}
            </Typography>
            <IconButton color="inherit">
              <Badge
                badgeContent={10}
                sx={{
                  "& .MuiBadge-badge": {
                    backgroundColor: "#FF1D1D",
                    color: "white",
                    fontSize: "7px",
                  },
                }}
              >
                <NotificationIcon />
              </Badge>
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "start",
              padding: "0 0 !important",
            }}
          >
            <IconButton
              onClick={toggleDrawer}
              sx={{ margin: "28px 0px 52px 50px" }}
            >
              <svg
                width={30}
                height={30}
                viewBox="0 0 30 30"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <rect width={30} height={30} rx={15} fill="#51B15C" />
                <path
                  d="M21.5 19.1381V20.5065H9.18421V19.1381H21.5ZM11.6446 9.49347L12.6121 10.4609L10.4349 12.6381L12.6121 14.8153L11.6446 15.7827L8.5 12.6381L11.6446 9.49347ZM21.5 14.3486V15.717H15.3421V14.3486H21.5ZM21.5 9.55915V10.9276H15.3421V9.55915H21.5Z"
                  fill="white"
                />
              </svg>
            </IconButton>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                gap: "10px",
                alignSelf: "center",
                marginBottom: "48px",
              }}
            >
              <Logo />
              <div
                style={{ fontWeight: 700, fontSize: "18px", color: "#51B15C" }}
              >
                Logo
              </div>
            </Box>
          </Toolbar>
          <List
            component="nav"
            sx={{
              padding: "0 35px 0 24px",
              display: "flex",
              flexDirection: "column",
              gap: "10px",
              "& .MuiButtonBase-root": {
                padding: "12px 22px 12px 22px",
                backgroundColor: "rgba(81, 177, 92, 0.04)",
                borderRadius: "2px 0px 0px 2px",
                borderLeft: "solid 3px rgba(81, 177, 92, 1)",
                "& .MuiListItemIcon-root": {
                  minWidth: "0px",
                  margin: "0px 10px 0px 0px",
                },
                "& .MuiListItemText-root": {
                  "& .MuiTypography-root": {
                    fontSize: "16px",
                    fontWeight: "600!important",
                    color: "rgba(81, 177, 92, 1)",
                  },
                },
              },
            }}
          >
            <SideBarList />
          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: "rgba(249, 249, 249, 1)",
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
          }}
        >
          <Outlet />
        </Box>
      </Box>
    </ThemeProvider>
  );
};
export default AdminLayout;
