import {
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  Navigate,
  Outlet,
} from "react-router-dom";
import { lazy } from "react";
import AdminLayout from "./layouts/admin-layout/admin-layout";
import Article from "./features/article/article";

const ArticleForm = lazy(() => import("./features/article/form/article-form"));
const ArticleList = lazy(() => import("./features/article/list/article-list"));

const Root = () => {
  return <Outlet />;
};

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Root />}>
      <Route path="" element={<Navigate to={"admin"} replace={true} />} />
      <Route path="admin" element={<AdminLayout />}>
        <Route
          path=""
          element={<Navigate to="article" replace={true} />}
        ></Route>
        <Route path="article" element={<Article />}>
          <Route path="" element={<ArticleList />}></Route>
          <Route path="create" element={<ArticleForm />}></Route>
          <Route path=":id" element={<Root />}>
            <Route path="edit" element={<ArticleForm />}></Route>
          </Route>
        </Route>
      </Route>
    </Route>
  )
);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
