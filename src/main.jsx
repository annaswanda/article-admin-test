import ReactDOM from "react-dom/client";
import App from "./app.jsx";
// import "@fontsource-variable/open-sans";
import "./index.css";
import { Provider } from "react-redux";
import store from "./app/store.js";

ReactDOM.createRoot(document.getElementById("root")).render(
  //   <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  //   </React.StrictMode>
);
