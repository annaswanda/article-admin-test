import { configureStore } from "@reduxjs/toolkit";
import articleListReducer from "../features/article/list/article-list-slice";
import articleFormReducer from "../features/article/form/article-form-slice";
import adminLayoutReducer from "../layouts/admin-layout/admin-layout-slice";
export default configureStore({
  reducer: {
    adminLayout: adminLayoutReducer,
    articleList: articleListReducer,
    articleForm: articleFormReducer,
  },
});
