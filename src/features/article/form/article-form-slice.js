import { createSlice } from "@reduxjs/toolkit";
import { REQUEST_STATUS } from "../../../common/utils";
import { findArticleById, storeArticle } from "./article-form-thunk";

export const articleFormSlice = createSlice({
  name: "article/form",
  initialState: {
    status: REQUEST_STATUS.SUCCESS,
    form: {
      id: null,
      title: "",
      content: "",
    },
  },
  reducers: {
    onArticleFormChange(state, payload) {
      state.form[payload.payload.name] = payload.payload.value;
    },
    resetArticleForm(state) {
      state.form = {
        id: null,
        title: "",
        content: "",
      };
      state.status = REQUEST_STATUS.SUCCESS;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(findArticleById.pending, (state, action) => {
      state.status = REQUEST_STATUS.LOADING;
    });
    builder.addCase(findArticleById.fulfilled, (state, action) => {
      state.status = REQUEST_STATUS.SUCCESS;
      state.form = {
        id: action.payload.data.id,
        title: action.payload.data.title,
        content: action.payload.data.content,
      };
    });
    builder.addCase(findArticleById.rejected, (state, action) => {
      state.status = REQUEST_STATUS.ERROR;
    });
    builder.addCase(storeArticle.pending, (state, action) => {
      state.status = REQUEST_STATUS.LOADING;
    });
    builder.addCase(storeArticle.fulfilled, (state, action) => {
      state.form = {
        id: null,
        title: "",
        content: "",
      };
      state.status = REQUEST_STATUS.SUCCESS;
    });
  },
});

export const { onArticleFormChange, changeArticleId, resetArticleForm } =
  articleFormSlice.actions;
export default articleFormSlice.reducer;
