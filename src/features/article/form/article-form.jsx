import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setAdminLayoutTitle } from "../../../layouts/admin-layout/admin-layout-slice";
import { Box } from "@mui/system";
import { Button, InputLabel, TextField, Typography } from "@mui/material";
import { ArticleTitleIcon } from "../../../components/icons/article-title-icon";
import { ArticleContentIcon } from "../../../components/icons/article-content-icon";
import { useNavigate, useParams } from "react-router";
import {
  onArticleFormChange,
  resetArticleForm,
} from "./article-form-slice";
import { findArticleById, storeArticle } from "./article-form-thunk";
import { REQUEST_STATUS } from "../../../common/utils";

const ArticleForm = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const articleForm = useSelector((state) => state.articleForm);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(resetArticleForm());
    if (id) {
      dispatch(findArticleById(id));
    }
    dispatch(
      setAdminLayoutTitle({
        title: "Article",
        subTitle: `${id ? "Edit" : "Create"} Article`,
      })
    );
  }, [id, dispatch]);
  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: "23px" }}>
      <Typography
        fontSize={"20px"}
        fontWeight={600}
        color="#171717"
        sx={{ borderBottom: "solid 1px #939393", pb: "15px" }}
      >
        {id ? "Edit" : "Create"} Article {id && `:: ${articleForm.form.title}`}
      </Typography>
      <Box>
        <InputLabel
          htmlFor="titleInput"
          sx={{ fontSize: "13px", mb: "6px", color: "#171717" }}
        >
          Titlle
        </InputLabel>
        <Box sx={{ position: "relative" }}>
          <Box
            sx={{
              display: "flex",
              position: "absolute",
              top: 0,
              left: 0,
              height: "100%",
              alignItems: "center",
              pl: "10px",
              zIndex: "10",
            }}
          >
            <ArticleTitleIcon />
          </Box>
          <TextField
            id="titleInput"
            variant="filled"
            InputProps={{ disableUnderline: true }}
            value={articleForm.form.title}
            onChange={(e) =>
              dispatch(
                onArticleFormChange({ name: "title", value: e.target.value })
              )
            }
            sx={{
              border: "none",
              backgroundColor: "#F7F7F7",
              "& .MuiInputBase-root": {
                pl: "20px",
                borderRadius: "8px",
                "& .MuiInputBase-input": {
                  padding: "15px 20px 15px 15px !important",
                },
              },
            }}
          />
        </Box>
      </Box>
      <Box>
        <InputLabel
          htmlFor="contentInput"
          sx={{ fontSize: "13px", mb: "6px", color: "#171717" }}
        >
          Content
        </InputLabel>
        <Box sx={{ position: "relative" }}>
          <Box
            sx={{
              display: "flex",
              position: "absolute",
              top: 16,
              left: 0,
              height: "100%",
              alignItems: "start",
              pl: "10px",
              zIndex: "10",
            }}
          >
            <ArticleContentIcon />
          </Box>
          <TextField
            multiline={true}
            id="contentInput"
            variant="filled"
            InputProps={{ disableUnderline: true }}
            value={articleForm.form.content}
            onChange={(e) =>
              dispatch(
                onArticleFormChange({ name: "content", value: e.target.value })
              )
            }
            sx={{
              border: "none",
              backgroundColor: "#F7F7F7",
              "& .MuiInputBase-root": {
                padding: "0px 0px 0px 20px",
                borderRadius: "8px",
                "& .MuiInputBase-input": {
                  padding: "15px 20px 15px 15px !important",
                },
              },
            }}
          />
        </Box>
      </Box>
      <Button
        variant="contained"
        onClick={async (e) => {
            const result =await dispatch(storeArticle(articleForm.form))
            result.type == "article-form/store/fulfilled" && navigate("/admin/article")
        }}
        sx={{
          mr: "auto",
          textTransform: "unset",
          color: "white",
          fontSize: "16px",
          fontWeight: 600,
          padding: "10px 30px 10px 30px",
          backgroundColor: "#51B15C",
          borderRadius: "8px",
          ":hover": {
            backgroundColor: "#61C16C",
          },
        }}
        disabled={articleForm.status == REQUEST_STATUS.LOADING}
      >
        Save
      </Button>
    </Box>
  );
};
export default ArticleForm;
