import { debounce } from "lodash";
import { api } from "../../../common/libs/api";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const findArticleByIdDebounced = debounce(async (id, cb) => {
  const { data } = await api.get(`articles/${id}`);
  cb(data);
}, 150);

export const storeArticleDebounced = debounce(async (_data, cb) => {
  if (_data.id) {
    const { data } = await api.put(`articles/${_data.id}`, _data);
    return cb(data);
  }
  const { data } = await api.post(`articles`, _data);
  return cb(data);
}, 150);

export const findArticleById = createAsyncThunk(
  "article-form/find-by-id",
  async (payload = 0) =>
    new Promise((res, rej) => {
      findArticleByIdDebounced(payload, (result) => res(result));
    })
);

export const storeArticle = createAsyncThunk(
  "article-form/store",
  async (payload = {}) =>
    new Promise((res, rej) => {
      storeArticleDebounced(payload, (result) => res(result));
    })
);
