import { createSlice } from "@reduxjs/toolkit";
import { REQUEST_STATUS } from "../../../common/utils";
import { destroyArticle, findAllArticle } from "./article-list-thunk";
import moment from "moment/moment";

export const articleListSlice = createSlice({
  name: "article/list",
  initialState: {
    param: {
      search: "",
      year: "2023",
      page_size: 10,
      page: 1,
    },
    data: [
      {
        id: 1,
        date: "20 June 1999",
        title: "",
        content: "",
      },
    ],
    totalPage: 1,
    status: REQUEST_STATUS.LOADING,
    confirmDeleteShow: false,
    selectedArticle: null,
  },
  reducers: {
    setArticleListLoading(state) {
      state.status = REQUEST_STATUS.LOADING;
    },
    searchArticle(state, payload) {
      state.param.search = payload.payload;
    },
    changePageArticle(state, payload) {
      state.param.page = payload.payload;
    },
    changePageSizeArticle(state, payload) {
      state.param.page_size = payload.payload;
    },
    changeYearArticle(state, payload) {
      state.param.year = payload.payload;
    },
    toggleDeleteArticleModal(state, payload) {
      state.confirmDeleteShow = !state.confirmDeleteShow;
      state.selectedArticle = state.confirmDeleteShow ? payload.payload : null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(findAllArticle.rejected, (state, action) => {
      state.status = REQUEST_STATUS.ERROR;
      alert("Gagal memuat artikel");
    });
    builder.addCase(findAllArticle.pending, (state, action) => {
      state.status = REQUEST_STATUS.LOADING;
    });
    builder.addCase(findAllArticle.fulfilled, (state, action) => {
      state.data = action.payload.data.articles.map((item) => ({
        id: item.id,
        title: item.title,
        content: item.content,
        date: moment(item.created_at).format("DD MMM YYYY"),
      }));
      state.totalPage = action.payload.data.page_info.last_page;
      state.status = REQUEST_STATUS.SUCCESS;
    });
    builder.addCase(destroyArticle.fulfilled, (state, action) => {
      state.data = state.data.filter((item) => item.id !== action.payload);
      state.confirmDeleteShow = false;
      state.selectedArticle = null;
    });
  },
});

export const {
  setArticleListLoading,
  searchArticle,
  changePageArticle,
  changePageSizeArticle,
  changeYearArticle,
  toggleDeleteArticleModal,
} = articleListSlice.actions;

export default articleListSlice.reducer;
