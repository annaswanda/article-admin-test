import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { REQUEST_STATUS } from "../../../common/utils";
import { destroyArticle, findAllArticle } from "./article-list-thunk";
import { setAdminLayoutTitle } from "../../../layouts/admin-layout/admin-layout-slice";
import {
  Alert,
  Box,
  Button,
  MenuItem,
  Modal,
  Pagination,
  Paper,
  Select,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import { SearchIcon } from "../../../components/icons/search-icon";
import { PlusIcon } from "../../../components/icons/plus-icon";
import { CalendarIcon } from "../../../components/icons/calendar-icon";
import { EditIcon } from "../../../components/icons/edit-icon";
import { TrashIcon } from "../../../components/icons/trash-icon";
import {
  changePageArticle,
  changePageSizeArticle,
  changeYearArticle,
  searchArticle,
  toggleDeleteArticleModal,
} from "./article-list-slice";
import { Link } from "react-router-dom";

const ArticleList = () => {
  const articleList = useSelector((state) => state.articleList);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(findAllArticle(articleList.param));
  }, [articleList.param, dispatch]);
  useEffect(() => {
    dispatch(
      setAdminLayoutTitle({ title: "Article", subTitle: "Article List" })
    );
  }, []);
  return (
    <Box>
      <Box
        sx={{ display: "flex", alignItems: "center", mb: "29px", gap: "10px" }}
      >
        <Box
          sx={{
            display: "flex",
            flexGrow: "1",
            gap: "10px",
            alignItems: "center",
            pl: "20px",
          }}
        >
          <SearchIcon />
          <TextField
            sx={{
              color: "#939393",
              flexGrow: 1,
              "& .MuiInputBase-root": {
                border: "unset !important",
              },
              "& .MuiInputBase-root::before": {
                border: "unset !important",
              },
              "& .MuiFormLabel-root": {
                fontSize: "10px",
              },
            }}
            id="standard-basic"
            label="Type here to search"
            variant="standard"
            onChange={(e) => dispatch(searchArticle(e.target.value))}
            value={articleList.param.search}
          />
        </Box>
        <Box sx={{ position: "relative" }}>
          <Box
            sx={{
              display: "flex",
              position: "absolute",
              alignItems: "center",
              top: 0,
              left: 0,
              height: "100%",
              zIndex: "20",
              pl: "10px",
            }}
          >
            <CalendarIcon />
          </Box>
          <Select
            value={articleList.param.year}
            onChange={(e) => dispatch(changeYearArticle(e.target.value))}
            sx={{
              height: "32px !important",
              backgroundColor: "#F7F7F7",
              borderRadius: "6px",
              padding: "6px 10px 6px 10px",
              fontSize: "8px",
              fontWeight: "600",
              "& .MuiOutlinedInput-notchedOutline": {
                border: "unset !important",
              },
            }}
          >
            <MenuItem>2023</MenuItem>
            <MenuItem>2022</MenuItem>
            <MenuItem>2021</MenuItem>
            <MenuItem value={2020}>2020</MenuItem>
          </Select>
        </Box>
        <Link to="create">
          <Button
            sx={{
              backgroundColor: "#51B15C",
              color: "white",
              display: "flex",
              alignItems: "center",
              gap: "10px",
              fontSize: "10px",
              fontWeight: "600",
              textTransform: "unset !important",
              ":hover": {
                backgroundColor: "#51B15C",
                color: "white",
              },
            }}
          >
            <PlusIcon />
            Add
          </Button>
        </Link>
      </Box>
      <TableContainer
        component={Paper}
        sx={{
          boxShadow: "unset !important",
          borderLeft: "solid 1px #939393",
          borderTop: "solid 1px #939393",
          borderRadius: "0px",
          "& .MuiTable-root": {},
        }}
      >
        <Table aria-label="simple table">
          <TableHead>
            <TableRow
              sx={{
                "& .MuiTableCell-root": {
                  borderBottom: "solid 1px #939393 !important",
                  borderRight: "solid 1px #939393 !important",
                  backgroundColor: "#EEF7EF",
                  color: "#51B15C",
                  fontSize: "10px",
                  fontWeight: "600",
                  padding: "10px !important",
                },
              }}
            >
              <TableCell align="center">Date</TableCell>
              <TableCell align="center">Tittle</TableCell>
              <TableCell align="center">Content</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody
            sx={{
              "& .MuiTableRow-root": {
                "& .MuiTableCell-root": {
                  borderBottom: "solid 1px #939393 !important",
                  borderRight: "solid 1px #939393 !important",
                  color: "#171717",
                  fontSize: "14px",
                  fontWeight: "400",
                  padding: "10px !important",
                },
              },
            }}
          >
            {articleList.status == REQUEST_STATUS.SUCCESS &&
              articleList.data.map((item) => (
                <TableRow
                  key={item.id}
                >
                  <TableCell align="center">{item.date}</TableCell>
                  <TableCell align="center">{item.title}</TableCell>
                  <TableCell align="center">{item.content}</TableCell>
                  <TableCell align="center">
                    <Box
                      sx={{
                        display: "flex",
                        gap: "10px",
                        justifyContent: "center",
                      }}
                    >
                      <Link to={`/admin/article/${item.id}/edit`}>
                        <Button
                          sx={{
                            borderRadius: "50%",
                            backgroundColor: "#CF8812",
                            width: "24px",
                            height: "24px",
                            minWidth: "unset !important",
                          }}
                        >
                          <EditIcon />
                        </Button>
                      </Link>
                      <Button
                        onClick={(e) =>
                          dispatch(toggleDeleteArticleModal(item))
                        }
                        sx={{
                          borderRadius: "50%",
                          backgroundColor: "#FF1D1D",
                          width: "24px",
                          height: "24px",
                          minWidth: "unset !important",
                        }}
                      >
                        <TrashIcon />
                      </Button>
                    </Box>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      {articleList.status == REQUEST_STATUS.LOADING && (
        <Skeleton variant="rectangular" width={`100%`} height={"40px"} />
      )}
      {articleList.status == REQUEST_STATUS.SUCCESS &&
        articleList.data.length == 0 && (
          <Alert severity="info" sx={{ mt: "10px", mx: "10px" }}>
            Data is empty!
          </Alert>
        )}
      <Box
        sx={{
          display: "flex",
          gap: "10px",
          justifyContent: "end",
          mt: "20px",
          alignItems: "center",
        }}
      >
        <Typography>Show</Typography>
        <Select
          value={articleList.param.page_size}
          onChange={(e) => dispatch(changePageSizeArticle(e.target.value))}
          sx={{ maxHeight: "30px" }}
        >
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={25}>25</MenuItem>
          <MenuItem value={100}>100</MenuItem>
        </Select>
        <Typography>Entries</Typography>
        <Pagination
          page={articleList.param.page}
          count={articleList.totalPage}
          shape="rounded"
          sx={{
            '& .Mui-selected' : {
                backgroundColor : '#51B15C',

                color : 'white',
                borderRadius : '4px'
            }
          }}
          onChange={(e, page) => dispatch(changePageArticle(page))}
        />
      </Box>
      <Modal
        open={articleList.confirmDeleteShow}
        sx={{
          display: "flex",
          "& .MuiBoxRoot-root": {
            margin: "auto",
          },
        }}
        onClose={(e) => {
          dispatch(toggleDeleteArticleModal());
        }}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            margin: "auto auto",
            backgroundColor: "white",
            borderRadius: "12px",
            padding: "32px 40px 32px 40px",
            display: "flex",
            flexDirection: "column",
            gap: "20px",
          }}
        >
          <Box sx={{ display: "flex", gap: "10px" }}>
            <Box
              sx={{
                width: "40px",
                height: "40px",
                backgroundColor: "#F7F7F7",
                borderRadius: "50%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <TrashIcon />
            </Box>
            <Typography fontSize={"20px"} fontWeight={600}>
              Delete Article
            </Typography>
          </Box>
          <Typography fontSize={"16px"} fontWeight={400}>
            Are you sure you want to delete it? You can’t undo this action.
          </Typography>
          <Box sx={{ ml: "auto", display: "flex", gap: "10px" }}>
            <Button
              onClick={(e) => dispatch(toggleDeleteArticleModal())}
              variant="text"
              sx={{ color: "#9E9D9D" }}
            >
              Cancel
            </Button>
            <Button
              onClick={(e) =>
                dispatch(destroyArticle(articleList.selectedArticle.id))
              }
              variant="contained"
              sx={{
                backgroundColor: "#F33A3A",
                borderRadius: "8px",
                padding: "10px 28px 10px 28px",
                color: "white",
              }}
            >
              Delete
            </Button>
          </Box>
        </Box>
      </Modal>
    </Box>
  );
};
export default ArticleList;
