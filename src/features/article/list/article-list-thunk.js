import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "../../../common/libs/api";
import { debounce } from "lodash";

const findAllArticleDebounced = debounce(async (payload = {}, cb) => {
  const { data } = await api.get("articles", { params: payload });
  cb(data);
}, 150);

const destroyArticleDebounced = debounce(async (payload, cb) => {
  await api.delete(`articles/${payload}`);
  cb();
});

export const findAllArticle = createAsyncThunk(
  "article-list/find-all",
  async (payload = {}) =>
    new Promise((res, rej) => {
      findAllArticleDebounced(payload, (result) => res(result));
    })
);
export const destroyArticle = createAsyncThunk(
  "article-list/destroy",
  async (payload = {}) =>
    new Promise((res, rej) => {
      destroyArticleDebounced(payload, () => res(payload));
    })
);
