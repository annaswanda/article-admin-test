import { Box, Container, Tab, Tabs, Toolbar, Typography } from "@mui/material";
import { Suspense, useEffect, useMemo, useState } from "react";
import { Outlet, useNavigate, useLocation, useMatch } from "react-router-dom";
import { ArticleListTabIcon } from "../../components/icons/article-list-tab-icon";
import { ArticleEditTabIcon } from "../../components/icons/article-edit-tab-icon";

const Article = () => {
  const navigate = useNavigate();
  const locationAtEdit = useMatch("/admin/article/:id/edit");
  const locationAtCreate = useMatch("/admin/article/create");
  const locationAtList = useMatch("/admin/article");
  const locationAt = useMemo(() => {
    if (locationAtList) return "/admin/article";
    if (locationAtCreate || locationAtEdit) return "/admin/article/create";
  }, [locationAtCreate, locationAtEdit, locationAtList]);
  const [value, setValue] = useState(locationAt);
  useEffect(() => {
    setValue(locationAt);
  }, [locationAt]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Toolbar />
      <Container
        maxWidth="xl"
        sx={{ mt: 4, mb: 4, ml: 0, padding: "30px 34px 30px 34px !important" }}
      >
        <Box sx={{ width: "100%" }}>
          <Box
            sx={{
              borderBottom: 1,
              borderColor: "divider",
              marginBottom: "20px",
            }}
          >
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
              sx={{
                "& .MuiButtonBase-root": {
                  minWidth: "259px",
                  alignItems: "start",
                  textTransform: "unset",
                },
                "& .MuiTabs-indicator": {
                  height: "2px",
                  backgroundColor: "#51B15C",
                  borderRadius: "0px 10px 0px 0px",
                },
                "& .MuiTypography-root": {
                  textAlign: "left",
                  ml: "10px",
                },
              }}
            >
              <Tab
                onClick={() => navigate("/admin/article")}
                value={"/admin/article"}
                label={
                  <Box sx={{ display: "flex", alignItems: "center" }}>
                    <ArticleListTabIcon active={value == "/admin/article"} />
                    <Box
                      sx={{
                        color: value == "/admin/article" ? "#51B15C" : "",
                      }}
                    >
                      <Typography fontSize={`14px`} fontWeight={600}>
                        Article
                      </Typography>
                      <Typography fontSize={`12px`} fontWeight={400}>
                        List Article
                      </Typography>
                    </Box>
                  </Box>
                }
              />
              <Tab
                onClick={() => navigate("/admin/article/create")}
                value={"/admin/article/create"}
                label={
                  <Box sx={{ display: "flex", alignItems: "center" }}>
                    <ArticleEditTabIcon
                      active={value == "/admin/article/create"}
                    />
                    <Box
                      sx={{
                        color:
                          value == "/admin/article/create" ? "#51B15C" : "",
                      }}
                    >
                      <Typography fontSize={`14px`} fontWeight={600}>
                        Add / Edit
                      </Typography>
                      <Typography fontSize={`12px`} fontWeight={400}>
                        Detail Article
                      </Typography>
                    </Box>
                  </Box>
                }
              />
            </Tabs>
          </Box>
          <Box
            sx={{
              borderRadius: "8px",
              backgroundColor: "white",
              padding: "20px",
            }}
          >
            <Suspense fallback={<>Loading...</>}>
              <Outlet />
            </Suspense>
          </Box>
        </Box>
      </Container>
    </>
  );
};
export default Article;
